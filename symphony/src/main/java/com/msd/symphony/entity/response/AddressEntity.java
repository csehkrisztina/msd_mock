package com.msd.symphony.entity.response;

import com.msd.symphony.dto.response.Address;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@Entity
@Table(name = "ADDRESS")
public class AddressEntity {

    @Id
    @Column(name = "address_id")
    private Long id;

    @Column(name = "house_name")
    private String houseName;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "location_supplement")
    private String locationSupplement;

    @Column(name = "location_id")
    private String locationId;

    @Column(name = "role")
    private String role;

    @Column(name = "validation_status")
    private String validationStatus;

    @Column(name = "street_name")
    private String streetName;

    @Column(name = "house_number")
    private String houseNumber;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @OneToOne
    @JoinColumn(name = "customer_id")
    private CustomerEntity customer;

    public static List<Address> toDtos(List<AddressEntity> addresses) {
        return addresses.stream().map(AddressEntity::toDto).collect(Collectors.toList());
    }

    public Address toDto() {
        Address dto = new Address();
        dto.setHouseName(houseName);
        dto.setZipCode(zipCode);
        dto.setLocationSupplement(locationSupplement);
        dto.setLocationId(locationId);
        dto.setRole(role);
        dto.setValidationStatus(validationStatus);
        dto.setStreetName(streetName);
        dto.setHouseNumber(houseNumber);
        dto.setCity(city);
        dto.setCountry(country);
        return dto;
    }

    public AddressEntity update(Address dto) {
        this.id = dto.getId();
        this.houseName = dto.getHouseName();
        this.zipCode = dto.getZipCode();
        this.locationSupplement = dto.getLocationSupplement();
        this.locationId = dto.getLocationId();
        this.role = dto.getRole();
        this.validationStatus = dto.getValidationStatus();
        this.streetName = dto.getStreetName();
        this.houseNumber = dto.getHouseNumber();
        this.city = dto.getCity();
        this.country = dto.getCountry();
        return this;
    }
}

