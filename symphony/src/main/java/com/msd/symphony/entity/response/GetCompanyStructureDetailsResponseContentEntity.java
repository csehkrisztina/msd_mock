package com.msd.symphony.entity.response;

import com.msd.symphony.dto.response.GetCompanyStructureDetailsResponseContent;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "GET_COMPANY_STRUCTURE_DETAILS_RESPONSE_CONTENT")
public class GetCompanyStructureDetailsResponseContentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "company_index")
    private String companyIndex;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "getCompanyStructureDetailsResponseContentEntity")
    private List<CustomerEntity> customers = new ArrayList<>();


    public static List<GetCompanyStructureDetailsResponseContent> toDtos(List<GetCompanyStructureDetailsResponseContentEntity> entities) {
        return entities.stream().map(GetCompanyStructureDetailsResponseContentEntity::toDto).collect(Collectors.toList());
    }

    public GetCompanyStructureDetailsResponseContent toDto() {
        GetCompanyStructureDetailsResponseContent dto = new GetCompanyStructureDetailsResponseContent();
        dto.setCustomer(CustomerEntity.toDtos(customers));
        dto.setCompanyIndex(companyIndex);
        return dto;
    }

    public GetCompanyStructureDetailsResponseContentEntity update(GetCompanyStructureDetailsResponseContent dto) {
        this.id = dto.getId();

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.update(customerEntity.toDto());

        this.companyIndex = dto.getCompanyIndex();
        return this;
    }
}
