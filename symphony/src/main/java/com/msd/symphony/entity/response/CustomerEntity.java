package com.msd.symphony.entity.response;

import com.msd.symphony.dto.response.Customer;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@Table(name = "CUSTOMER")
@Entity
public class CustomerEntity {

    @Id
    @Column(name = "customer_id")
    private Long id;

    @Column(name = "status")
    private String status;

    @OneToOne(mappedBy = "customer")
    private AddressEntity address;

    @OneToOne(mappedBy = "customer")
    private OrganizationUnitEntity organizationUnit;

    @OneToOne(mappedBy = "customer")
    private IdentifierEntity identifier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "get_company_structure_details_response_content_id")
    private GetCompanyStructureDetailsResponseContentEntity getCompanyStructureDetailsResponseContentEntity;


    public static List<Customer> toDtos(List<CustomerEntity> customers) {
        return customers.stream().map(CustomerEntity::toDto).collect(Collectors.toList());
    }

    public Customer toDto() {
        Customer dto = new Customer();
        dto.setAddress(address.toDto());
        dto.setIdentifier(identifier.toDto());
        dto.setOrganizationUnit(organizationUnit.toDto());
        return dto;
    }

    public CustomerEntity update(Customer dto) {
        this.id = dto.getId();
        this.status = dto.getStatus();

        AddressEntity addressEntity = new AddressEntity();
        addressEntity.update(dto.getAddress());
        this.address = addressEntity;

        OrganizationUnitEntity organizationUnitEntity = new OrganizationUnitEntity();
        organizationUnitEntity.update(dto.getOrganizationUnit());
        this.organizationUnit = organizationUnitEntity;

        IdentifierEntity identifierEntity = new IdentifierEntity();
        identifierEntity.update(dto.getIdentifier());
        this.identifier = identifierEntity;
        return this;
    }
}
