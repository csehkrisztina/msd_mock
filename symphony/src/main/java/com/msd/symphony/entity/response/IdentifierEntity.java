package com.msd.symphony.entity.response;

import com.msd.symphony.dto.response.Identifier;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "IDENTIFIER")
public class IdentifierEntity {

    @Id
    @Column(name = "identifier_id")
    private Long id;

    @Column(name = "value")
    private String value;

    @OneToOne
    @JoinColumn(name = "customer_id")
    private CustomerEntity customer;

    public static List<Identifier> toDtos(List<IdentifierEntity> identifiers) {
        return identifiers.stream().map(IdentifierEntity::toDto).collect(Collectors.toList());
    }

    public Identifier toDto() {
        Identifier dto = new Identifier();
        dto.setValue(value);
        return dto;
    }

    public IdentifierEntity update(Identifier dto) {
        this.id = dto.getId();
        this.value = dto.getValue();
        return this;
    }
}
