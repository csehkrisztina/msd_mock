package com.msd.symphony.entity.response;

import com.msd.symphony.dto.response.OrganizationName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "ORGANIZATION_NAME")
public class OrganizationNameEntity {

    @Id
    @Column(name = "organization_name_id")
    private Long id;

    @Column(name = "trading_name")
    private String tradingName;

    @OneToOne
    @JoinColumn(name = "organization_unit_id")
    private OrganizationUnitEntity organizationUnit;

    public static List<OrganizationName> toDtos(List<OrganizationNameEntity> entities){
        return entities.stream().map(OrganizationNameEntity::toDto).collect(Collectors.toList());
    }

    public OrganizationName toDto() {
        OrganizationName dto = new OrganizationName();
        dto.setTradingName(tradingName);
        return dto;
    }

    public OrganizationNameEntity update(OrganizationName dto) {
        this.id = dto.getId();
        this.tradingName = dto.getTradingName();
        return this;
    }
}