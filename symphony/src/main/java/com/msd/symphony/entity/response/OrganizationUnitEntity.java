package com.msd.symphony.entity.response;

import com.msd.symphony.dto.response.OrganizationUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "ORGANIZATION_UNIT")
public class OrganizationUnitEntity {

    @Id
    @Column(name = "organization_unit_id")
    private Long id;

    @OneToOne(mappedBy = "organizationUnit")
    private OrganizationNameEntity organizationName;

    @Column(name = "head_quarter")
    private String headQuarter;

    @OneToOne
    @JoinColumn(name = "customer_id")
    private CustomerEntity customer;

    public static List<OrganizationUnit> toDtos(List<OrganizationUnitEntity> entities) {
        return entities.stream().map(OrganizationUnitEntity::toDto).collect(Collectors.toList());
    }

    public OrganizationUnit toDto() {
        OrganizationUnit dto = new OrganizationUnit();
        dto.setOrganizationName(organizationName.toDto());
        dto.setHeadQuarter(headQuarter);
        return dto;
    }

    public OrganizationUnitEntity update(OrganizationUnit dto) {
        this.id = dto.getId();
        this.headQuarter = dto.getHeadQuarter();

        OrganizationNameEntity organizationNameEntity = new OrganizationNameEntity();
        organizationNameEntity.update(dto.getOrganizationName());
        this.organizationName = organizationNameEntity;

        return this;
    }
}