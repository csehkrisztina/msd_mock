package com.msd.symphony.controller;

import com.msd.symphony.dto.request.GetCompanyStructureDetailsRequest;
import com.msd.symphony.dto.response.GetCompanyStructureDetailsResponseContent;
import com.msd.symphony.entity.response.GetCompanyStructureDetailsResponseContentEntity;
import com.msd.symphony.repository.GetCompanyStructureDetailsResponseContentEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@RestController(value = "/api/symphony")
public class GetCompanyStructureDetailsResponseContentController {

    private static final String TOKEN = "AbCdEf123456";
    private static final String BAD_TOKEN = "DFrgEf123456";

    @Autowired
    GetCompanyStructureDetailsResponseContentEntityRepository repository;

    @RequestMapping(value = "/xml", method = RequestMethod.POST, produces = {MediaType.APPLICATION_ATOM_XML_VALUE}, consumes = {MediaType.APPLICATION_ATOM_XML_VALUE})
    public ResponseEntity<GetCompanyStructureDetailsResponseContent> getXml(@RequestHeader(value = "Authorization") String authorization, @RequestBody GetCompanyStructureDetailsRequest request, HttpServletResponse response) throws IOException {

        String companyIndex = request.getGetCompanyStructureDetailsRequestContent().getCustomer().getOrganizationIdentifier().getCompanyIndex();

        return getGetCompanyStructureDetailsResponseContentResponseEntity(authorization, response, companyIndex);

    }

    private ResponseEntity<GetCompanyStructureDetailsResponseContent> getGetCompanyStructureDetailsResponseContentResponseEntity(@RequestHeader(value = "Authorization") String authorization, HttpServletResponse response, String companyIndex) throws IOException {
        if(authorization == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else if (authorization.equals(TOKEN)) {
            Optional<GetCompanyStructureDetailsResponseContentEntity> optional = repository.findByCompanyIndex(companyIndex);
            return optional.map(getCompanyStructureDetailsResponseContentEntity -> ResponseEntity.ok(getCompanyStructureDetailsResponseContentEntity.toDto())).orElseGet(() -> ResponseEntity.noContent().build());
        } else if (authorization.equals(BAD_TOKEN)) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else if (authorization.isEmpty()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

}
