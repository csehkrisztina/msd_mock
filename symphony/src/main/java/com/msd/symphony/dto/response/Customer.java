package com.msd.symphony.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Customer {

    private Long id;
    @XmlElement(name = "ns0:identifier")
    private Identifier identifier;
    @XmlElement(name = "ns0:status")
    private String status;
    @XmlElement(name = "ns0:organizationUnit")
    private OrganizationUnit organizationUnit;
    @XmlElement(name = "ns0:address")
    private Address address;
}
