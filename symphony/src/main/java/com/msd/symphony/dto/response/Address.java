package com.msd.symphony.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "address")
@XmlAccessorType(XmlAccessType.FIELD)
public class Address {

    private Long id;
    @XmlAttribute
    private String role;
    @XmlElement(name = "ns0:locationId")
    private String locationId;
    @XmlElement(name = "ns0:zipCode")
    private String zipCode;
    @XmlElement(name = "ns0:validationStatus")
    private String validationStatus;
    @XmlElement(name = "ns0:city")
    private String city;
    @XmlElement(name = "ns0:streetName")
    private String streetName;
    @XmlElement(name = "ns0:country")
    private String country;
    @XmlElement(name = "ns0:houseNumber")
    private String houseNumber;
    @XmlElement(name = "ns0:houseName")
    private String houseName;
    @XmlElement(name = "ns0:locationSupplement")
    private String locationSupplement;
}

