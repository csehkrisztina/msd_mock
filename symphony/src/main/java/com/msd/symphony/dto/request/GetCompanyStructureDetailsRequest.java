package com.msd.symphony.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name="v1:getCompanyStructureDetailsRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetCompanyStructureDetailsRequest {

    @XmlElement(name="v1:trackingInformation")
    private TrackingInformation trackingInformation;
    @XmlElement(name="v1:getCompanyStructureDetailsRequestContent")
    private GetCompanyStructureDetailsRequestContent getCompanyStructureDetailsRequestContent;
}
