package com.msd.symphony.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrackingInformation {

    @XmlElement(name="typ:eventLocalID")
    private String eventLocalID;

    @XmlElement(name="typ:applicationID")
    private String applicationID;

    @XmlElement(name="typ:originApplicationID")
    private String originApplicationID;
}
