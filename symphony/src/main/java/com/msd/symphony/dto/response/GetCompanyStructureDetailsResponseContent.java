package com.msd.symphony.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "ns0:getCompanyStructureDetailsResponseContent")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetCompanyStructureDetailsResponseContent {

    private Long id;
    @XmlElement(name = "ns0:companyIndex")
    private String companyIndex;
    @XmlElement(name = "ns0:customer")
    private List<Customer> customer;
}
