package com.msd.symphony.service;

import com.msd.symphony.entity.response.GetCompanyStructureDetailsResponseContentEntity;
import com.msd.symphony.repository.GetCompanyStructureDetailsResponseContentEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GetCompanyStructureDetailsResponseContentServiceImpl implements GetCompanyStructureDetailsResponseContentService {

    @Autowired
    GetCompanyStructureDetailsResponseContentEntityRepository repository;

    @Override
    public Optional<GetCompanyStructureDetailsResponseContentEntity> findByCompanyIndex(String companyIndex) {
        return repository.findByCompanyIndex(companyIndex);
    }
}
