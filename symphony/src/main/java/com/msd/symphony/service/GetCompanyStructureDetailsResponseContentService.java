package com.msd.symphony.service;

import com.msd.symphony.entity.response.GetCompanyStructureDetailsResponseContentEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface GetCompanyStructureDetailsResponseContentService {

    Optional<GetCompanyStructureDetailsResponseContentEntity> findByCompanyIndex(String companyIndex);
}
