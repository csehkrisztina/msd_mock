package com.msd.symphony.repository;

import com.msd.symphony.entity.response.GetCompanyStructureDetailsResponseContentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GetCompanyStructureDetailsResponseContentEntityRepository extends CrudRepository<GetCompanyStructureDetailsResponseContentEntity, Long> {

    Optional<GetCompanyStructureDetailsResponseContentEntity> findByCompanyIndex(String companyIndex);
}
