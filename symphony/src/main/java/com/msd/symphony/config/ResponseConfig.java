package com.msd.symphony.config;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

@Configuration
public class ResponseConfig {

    @Bean
    public ErrorAttributes errorAttributes() {
        return new DefaultErrorAttributes() {

            @Override
            public Map<String, Object> getErrorAttributes(WebRequest requestAttributes, boolean includeStackTrace) {
                Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);
                Integer status = Integer.valueOf(errorAttributes.get("status").toString());
                errorAttributes.remove("path");
                errorAttributes.remove("timestamp");
                errorAttributes.remove("message");
                errorAttributes.remove("error");
                if (status == 400) {
                    errorAttributes.put("code", "INVALID_REQUEST");
                    errorAttributes.put("message", "The request could not be understood due to malformed syntax. Correct the request syntax and try again.");
                } else if (status == 401) {
                    errorAttributes.put("code", "INVALID_AUTHENTICATION_CREDENTIALS");
                    errorAttributes.put("message", "Authentication credentials missing or incorrect. Check that you are using the correct authentication method and that you have permission to the given resource and your app key is approved.");
                } else if (status == 403) {
                    errorAttributes.put("code", "FORBIDDEN_RESOURCE");
                    errorAttributes.put("message", "The server understood the request, but is refusing to fulfill it. Check that you have permission to the given resource.");
                } else if (status == 404) {
                    errorAttributes.put("code", "INVALID_REQUEST_RESOURCE");
                    errorAttributes.put("message", "The requested resource does not exist. Verify that the resource URI is correctly spelled.");
                } else if (status == 405) {
                    errorAttributes.put("code", "INVALID_REQUEST_RESOURCE");
                    errorAttributes.put("message", "The resource is not accessible via the given method. Check the documentation or the Allow header for allowed methods.");
                } else if (status == 406) {
                    errorAttributes.put("code", "INVALID_HEADER_ACCEPT");
                    errorAttributes.put("message", "The requested resource is only capable of generating content not acceptable according to the Accept headers sent in the request. Make sure the request has an Accept header with a media type supported by the API. Check the developer portal for supported media types.");
                } else if (status == 429) {
                    errorAttributes.put("code", "EXPIRED_QUOTA");
                    errorAttributes.put("message", "Your quota for this service has been exhausted. Try again later or contact support at developer-support@swisscom.com.");
                } else if (status == 500) {
                    errorAttributes.put("code", "INTERNAL_SERVER_ERROR");
                    errorAttributes.put("message", "The server encountered an unexpected condition which prevented it from fulfilling the request. Try again later or contact support at developer-support@swisscom.com.");
                } else if (status == 503) {
                    errorAttributes.put("code", "UNAVAILABLE_SERVICE");
                    errorAttributes.put("message", "The server is currently unable to handle the request due to temporary overloading or maintenance. Try again later or contact support at developer-support@swisscom.com.");
                }
                errorAttributes.put("detail", "");
                return errorAttributes;
            }

        };
    }
}
