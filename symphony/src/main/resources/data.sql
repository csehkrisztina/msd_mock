INSERT INTO GET_COMPANY_STRUCTURE_DETAILS_RESPONSE_CONTENT(id, company_index) VALUES
(10, '33503'),
(11, 'ttttt');

INSERT INTO CUSTOMER (customer_id, status, get_company_structure_details_response_content_id) VALUES
(100, 'sts2322', 10),
(101, 'sts232322', 10),
(102, 'sts23f3422', 10),
(103, 'sts212322', 10),
(104, 'sts243322', 10),

(105, 'sts23411422322', 11),
(106, 'sts23412352322', 11),
(107, 'stkkkk412322', 11);


INSERT INTO ADDRESS (address_id, house_name, zip_code, location_supplement, location_id, role, validation_status, street_name, house_number, city, country, customer_id) VALUES
(1000, 'house1', 'zipcode1', 'locationsupplenemt23328', '7990', 'main', 'validated', 'Brassenstrus', '10', 'BERN', 'Switzerland', 100),
(1001, 'house2', 'zipcode2', 'locationsupplenemt231232', '7992', 'main', 'validated', 'Brassenstrus', '130', 'Zurich', 'Switzerland', 101),
(1002, 'house3', 'zipcode3', 'locationsupplenemt231332', '7995', 'main', 'validated', 'Brassenstrus', '1610', 'Geneva', 'Switzerland', 102),
(1003, 'house4', 'zipcode12', 'locationsupplenemt24332', '7996', 'main', 'validated', 'Brassenstrus', '170', 'Will', 'Switzerland', 103),
(1004, 'house5', 'zipcode16', 'locationsupplenemt26332', '7997', 'main', 'validated', 'Brassenstrus', '610', 'TUN', 'Switzerland', 104),

(1005, 'house6', 'zipcode21', 'locationsupplenemt239032', '7998', 'main', 'validated', 'Brassenstrus', '130', 'Linchestein', 'Linchestein', 105),
(1006, 'house7', 'zipcode14', 'locationsupplenemt23-32', '7999', 'main', 'validated', 'Brassenstrus', '120', 'Lille', 'France', 106),
(1007, 'house8', 'zipcode16', 'locationsupplenemt27332', '7993', 'main', 'validated', 'Brassenstrus', '105', 'Lyon', 'France', 107);


INSERT INTO ORGANIZATION_UNIT (organization_unit_id, head_quarter, customer_id) VALUES
(10000, 'hhh', 100),
(10001, 'hhh', 101),
(10002, 'hhh', 102),
(10003, 'hhh', 103),
(10004, 'hhh', 104),

(10005, 'hhh', 105),
(10006, 'hhh', 106),
(10007, 'hhh', 107);

INSERT INTO ORGANIZATION_NAME (organization_name_id, trading_name, organization_unit_id) VALUES
(200, 'trade', 10000),
(201, 'trade', 10001),
(202, 'trade', 10002),
(203, 'trade', 10003),
(204, 'trade', 10004),

(205, 'trade', 10005),
(206, 'trade', 10006),
(207, 'trade', 10007);

INSERT INTO IDENTIFIER (identifier_id, value, customer_id) VALUES
(100000, 'someValue100', 100),
(100001, 'someValue102', 101),
(100002, 'someValue103', 102),
(100003, 'someValue105', 103),
(100004, 'someValue124', 104),

(100005, 'someValue140', 105),
(100006, 'someValue180', 106),
(100007, 'someValue111', 107);