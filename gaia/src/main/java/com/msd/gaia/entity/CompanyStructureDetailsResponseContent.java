package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "ns0:getCompanyStructureDetailsResponseContent")
@XmlAccessorType(XmlAccessType.FIELD)
public class CompanyStructureDetailsResponseContent extends BaseEntity {

    @OneToMany(mappedBy="companyStructureDetailsResponseContent")
    @JsonProperty(value = "ns0:customer")
    private List<Customer> customers;

    @JsonIgnore
    private String companyIndex;


}
