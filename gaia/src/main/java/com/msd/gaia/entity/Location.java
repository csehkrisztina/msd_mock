package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "LOCATION")
@Getter
@Setter
@NoArgsConstructor
//@JsonPropertyOrder({"addressDetail", "numberOfAPP" })
public class Location extends BaseEntity {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addressDetail_id")
    @JsonIgnore
    private AddressDetail addressDetail;

    @Size(min = 1)
    public Integer addressState;
    @Size(min = 1)
    public String addressUsage;
    @Size(min = 1)
    public Integer addressType;
    @Size(min = 1)
    public String streetName;
    @Size(min = 1)
    public String streetShortName;
    @Size(min = 1)
    public String streetLanguage;
    public String streetNameSnd;
    public String streetShortNameSnd;
    public String streetLanguageSnd;
    public Integer houseNumber;
    public String houseName;
    public Integer numberOfAppartBfS;
    public Integer numberOfAppartPost;
    @JsonProperty(value = "numberOfAppartPartnerPrivate")
    public Integer numberOfApp;
    @JsonProperty(value = "numberOfAppartPartnerBusiness")
    public Integer numberOfApb;
    @JsonProperty(value = "numberOfAppartSwisscomPrivate")
    public Integer numberOfAsp;
    @JsonProperty(value = "numberOfAppartSwisscomBusiness")
    public Integer numberOfAsb;
    public String bestCoordinates;
    @Size(min = 1)
    public String supplierCode;
    public String secondSupplierCode;
    @Size(min = 1)
    public String egidUnique;
    public String Position;


}
