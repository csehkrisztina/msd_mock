package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName(value = "v1:trackingInformation", namespace = "http://www.swisscom.ch/eai/bsl/generic/types")
public class TrackingInformation {

    @JsonProperty(value = "typ:eventLocalID")
    public String eventLocalID;
    @JsonProperty(value = "typ:applicationID")
    public String applicationID;
    @JsonProperty(value = "typ:originApplicationID")
    public String originApplicationID;

}

