package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "ns0:address", namespace = "role=\"main\"")
@XmlAccessorType(XmlAccessType.FIELD)
public class SymphonyAddress extends BaseEntity {

    @JsonProperty(value = "ns0:locationId")
    public Long locationId;

    @JsonProperty(value = "ns0:zipCode")
    public Integer zipCode;

    @JsonProperty(value = "ns0:validationStatus")
    public Integer validationStatus;

    @JsonProperty(value = "ns0:city")
    public String city;
    @JsonProperty(value = "ns0:streetName")
    public String streetName;

    @JsonProperty(value = "ns0:country")
    public String country;

    @JsonProperty(value = "ns0:houseNumber")
    public Integer houseNumber;

    @JsonProperty(value = "ns0:houseName")
    public String houseName;

    @JsonProperty(value = "ns0:locationSupplement")
    public String locationSupplement;

    @JsonIgnore
    public Long uniqueAddressId;

    @JsonIgnore
    public String label;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    @JsonIgnore
    private Customer customer;
}
