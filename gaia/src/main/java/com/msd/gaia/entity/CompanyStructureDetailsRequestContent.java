package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@XmlRootElement(name = "v1:getCompanyStructureDetailsRequestContent")
@JsonRootName(value = "v1:getCompanyStructureDetailsRequestContent")
@XmlAccessorType(XmlAccessType.FIELD)
public class CompanyStructureDetailsRequestContent {

    @JsonProperty(value = "v1:customer")
    @XmlElement
    public Customer customer;

}
