package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "v1:getCompanyStructureDetailsRequest", namespace = "http://www.swisscom.ch/eai/bsl/customer/v1-0")
@XmlAccessorType(XmlAccessType.FIELD)
public class CompanyStructureDetailsRequest {

    @JsonProperty(value = "v1:trackingInformation")
    public TrackingInformation trackingInformation;
    @JsonProperty(value = "v1:getCompanyStructureDetailsRequestContent")
    public CompanyStructureDetailsRequestContent companyStructureDetailsRequestContent;
}
