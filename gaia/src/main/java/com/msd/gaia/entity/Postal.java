package com.msd.gaia.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Postal extends BaseEntity {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addressDetail_id")
    @JsonIgnore
    private AddressDetail addressDetail;

    private String region;

    private Integer numberOfAppart;

    private String zipType;

    private String zipCode6;

    private String cantonCode;

    private String streetNameSnd;

    private String placeShortName;

    private Integer houseNumber;

    private String placeName;

    private String streetName;

    private String streetLanguageSnd;

    private String country;

    private String municipalityName;

    private String geoPostKey;

    private String houseName;

    private String streetLanguage;

    private Long coordinates;

    private String municipalityNr;
}
