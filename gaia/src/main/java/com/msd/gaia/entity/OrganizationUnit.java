package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class OrganizationUnit extends BaseEntity {

    @OneToOne(mappedBy = "organizationUnit", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    @JsonProperty(value = "ns0:organizationName")
    private OrganizationName organizationName;

    @JsonProperty(value = "ns0:headQuarter")
    private Boolean headQuarter;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    @JsonIgnore
    private Customer customer;
}
