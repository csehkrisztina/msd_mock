package com.msd.gaia.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDetail extends BaseEntity {

    @OneToOne(mappedBy = "addressDetail", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Detail address;

    @OneToOne(mappedBy = "addressDetail", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Location location;

    @OneToOne(mappedBy = "addressDetail", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Bfs bfs;

    @OneToOne(mappedBy = "addressDetail", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Postal postal;

}
