package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Customer extends BaseEntity {

    @OneToOne(mappedBy = "customer", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    @JsonProperty(value = "ns0:identifier")
    private Identifier identifier;

    @JsonProperty(value = "ns0:status")
    private String status;

    @OneToOne(mappedBy = "customer", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    @JsonProperty(value = "ns0:organizationUnit")
    private OrganizationUnit organizationUnit;

    @OneToOne(mappedBy = "customer", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    @JsonProperty(value = "ns0:address")
    private SymphonyAddress address;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "companyStructureDetailsResponseContent_id", nullable = false)
    @JsonIgnore
    private CompanyStructureDetailsResponseContent companyStructureDetailsResponseContent;

}
