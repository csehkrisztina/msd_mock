package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Bfs extends BaseEntity {


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addressDetail_id")
    @JsonIgnore
    private AddressDetail addressDetail;

    public Long edid;

    public Integer numberOfAppart;

    public String gegrid;

    public String egid;

    public String cantonCode;

    public String streetNameSnd;

    public Integer houseNumber;

    public Integer lotNumber;

    public String streetName;

    public String streetLanguageSnd;

    public String municipalityName;

    public Integer landRegisterNumber;

    public String houseName;

    public String streetLanguage;

    public Long coordinates;

    public String municipalityNr;
}
