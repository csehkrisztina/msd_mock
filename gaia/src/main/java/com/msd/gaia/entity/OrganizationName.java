package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class OrganizationName extends BaseEntity {

    @JsonProperty(value = "ns0:tradingName")
    private String tradingName;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organizationUnit_id")
    @JsonIgnore
    private OrganizationUnit organizationUnit;
}
