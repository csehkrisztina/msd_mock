package com.msd.gaia.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Detail extends BaseEntity {

    public String addressLine1;
    public String addressLine2;
    public String addressLine3;
    public Long uniqueAddressId;
    public Long bestAddressId;
    public String country;
    public String countryCode;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addressDetail_id")
    @JsonIgnore
    private AddressDetail addressDetail;


}
