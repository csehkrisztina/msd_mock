package com.msd.gaia.controller;

import com.msd.gaia.entity.AddressDetail;
import com.msd.gaia.entity.AddressLine;
import com.msd.gaia.service.GaiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@ControllerAdvice
@RestController
@RequestMapping(value = "/api/location")
public class GaiaController extends ResponseEntityExceptionHandler {

    private static final String TOKEN = "AbCdEf123456";
    private static final String BAD_TOKEN = "DFrgEf123456";
    private static final String USER_ID = "MsdGaia";

    @Autowired
    public GaiaService gaiaService;


    @GetMapping(value = "/addressLine/search")
    public ResponseEntity<AddressLine> getAddresses(@RequestHeader(value = "Authorization") String authorization,
                                                    @RequestParam("userId") String userId,
                                                    @RequestParam("searchText") String searchText,
                                                    @RequestParam("maxResults") int maxResults,
                                                    HttpServletResponse response) throws IOException {
        return getAddressLineResponseEntity(authorization, userId, searchText, maxResults, response);
    }

    @GetMapping(value = "/addressDetails")
    @ExceptionHandler(UnsatisfiedServletRequestParameterException.class)
    public ResponseEntity<AddressDetail> getAddressDetails(@RequestHeader(value = "Authorization") String authorization,
                                                           @RequestParam("uniqueId") String uniqueId,
                                                           @RequestParam("userId") String userId,
                                                           HttpServletResponse response) throws IOException {
        return getAddressDetailResponse(authorization, uniqueId, userId, response);
    }

    private ResponseEntity<AddressDetail> getAddressDetailResponse(@RequestHeader(value = "Authorization") String authorization,
                                                                   @RequestParam("uniqueId") String uniqueId,
                                                                   @RequestParam("userId") String userId,
                                                                   HttpServletResponse response) throws IOException {
        if (authorization == null || userId == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else if (authorization.equals(TOKEN) && userId.equals(USER_ID)) {
            AddressDetail addressDetail = gaiaService.getByUnique(uniqueId);
            return getAddressDetailResponseEntity(uniqueId, response, addressDetail);
        } else if (authorization.equals(BAD_TOKEN)) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else if (authorization.isEmpty() || userId.isEmpty()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    private ResponseEntity<AddressDetail> getAddressDetailResponseEntity(@RequestParam("uniqueId") String uniqueId,
                                                                         HttpServletResponse response,
                                                                         AddressDetail addressDetail) throws IOException {
        if (addressDetail != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json; charset=utf-8");
            return new ResponseEntity<>(gaiaService.getByUnique(uniqueId), headers, HttpStatus.OK);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private ResponseEntity<AddressLine> getAddressLineResponseEntity(@RequestHeader(value = "Authorization") String authorization,
                                                                     @RequestParam("userId") String userId,
                                                                     @RequestParam("searchText") String searchText,
                                                                     @RequestParam("maxResults") int maxResults,
                                                                     HttpServletResponse response) throws IOException {
        if (authorization == null || userId == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else if (authorization.equals(TOKEN) && userId.equals(USER_ID)) {
            AddressLine addressLine = gaiaService.getResponesFrom1LineAddress(searchText, maxResults);
            return new ResponseEntity<>(addressLine, HttpStatus.OK);
        } else if (authorization.equals(BAD_TOKEN)) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else if (authorization.isEmpty() || userId.isEmpty()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "getAll")
    public Iterable<AddressDetail> getAll() {
        return gaiaService.getAll();
    }
}
