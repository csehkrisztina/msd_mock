package com.msd.gaia.controller;

import com.msd.gaia.entity.CompanyStructureDetailsResponseContent;
import com.msd.gaia.service.GaiaService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.w3c.dom.Document;
import org.xml.sax.*;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import java.io.*;

@ControllerAdvice
@RestController
@RequestMapping(value = "/api/symphony")
public class SymphonyController extends ResponseEntityExceptionHandler {

    private static final String TOKEN = "AbCdEf123456";
    private static final String BAD_TOKEN = "DFrgEf123456";

    @Autowired
    public GaiaService gaiaService;

    @PostMapping(value = "/customer/lookup", produces = {MediaType.APPLICATION_ATOM_XML_VALUE})
    public ResponseEntity<CompanyStructureDetailsResponseContent> getAddresses(@RequestHeader(value = "Authorization") String authorization,
                                                                               @RequestBody String customerSearch, HttpServletResponse response) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {

        String companyIndex = getCompanyIndexFromXml(customerSearch);

        return getCompanyStructureDetailsResponseContentResponseEntity(authorization, response, companyIndex);

    }

    private ResponseEntity<CompanyStructureDetailsResponseContent> getCompanyStructureDetailsResponseContentResponseEntity(@RequestHeader(value = "Authorization") String authorization, HttpServletResponse response, String companyIndex) throws IOException {
        if (authorization == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else if (authorization.equals(TOKEN)) {
            CompanyStructureDetailsResponseContent companyStructureDetailsResponseContent = gaiaService.getReponseFromSymphony(companyIndex);
            return new ResponseEntity<>(companyStructureDetailsResponseContent, HttpStatus.OK);
        } else if (authorization.equals(BAD_TOKEN)) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else if (authorization.isEmpty()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    private String getCompanyIndexFromXml(@RequestBody String customerSearch) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(customerSearch));
        Document doc = documentBuilder.parse(is);

        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = (XPath) xpathFactory.newXPath();
        XPathExpression expr = xpath.compile("//*[local-name()='companyIndex']");
        return (String) expr.evaluate(doc, XPathConstants.STRING);
    }
}
