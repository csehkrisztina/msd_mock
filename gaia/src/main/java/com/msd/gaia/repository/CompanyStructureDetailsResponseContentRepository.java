package com.msd.gaia.repository;

import com.msd.gaia.entity.CompanyStructureDetailsResponseContent;
import org.springframework.data.repository.CrudRepository;

public interface CompanyStructureDetailsResponseContentRepository extends CrudRepository<CompanyStructureDetailsResponseContent, Long> {

    CompanyStructureDetailsResponseContent findByCompanyIndex(String companyIndex);
}
