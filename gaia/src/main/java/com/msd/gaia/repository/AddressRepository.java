package com.msd.gaia.repository;

import com.msd.gaia.entity.Address;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {

    List<Address> findByLabelIgnoreCaseContaining(String label, Pageable page);
}
