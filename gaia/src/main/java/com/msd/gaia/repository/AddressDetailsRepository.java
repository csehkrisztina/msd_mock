package com.msd.gaia.repository;

import com.msd.gaia.entity.AddressDetail;
import org.springframework.data.repository.CrudRepository;

public interface AddressDetailsRepository extends CrudRepository<AddressDetail, Long> {

    AddressDetail findByAddressUniqueAddressId(Long uniqueAddressId);
}
