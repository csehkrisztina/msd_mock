package com.msd.gaia.service;

import com.msd.gaia.entity.Address;
import com.msd.gaia.entity.AddressDetail;
import com.msd.gaia.entity.AddressLine;
import com.msd.gaia.entity.CompanyStructureDetailsResponseContent;
import com.msd.gaia.repository.AddressDetailsRepository;
import com.msd.gaia.repository.AddressRepository;
import com.msd.gaia.repository.CompanyStructureDetailsResponseContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GaiaService {

    public ArrayList<Address> addresses = new ArrayList<>();

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressDetailsRepository addressDetailsRepository;

    @Autowired
    private CompanyStructureDetailsResponseContentRepository companyStructureDetailsResponseContentRepository;

    public AddressLine getResponesFrom1LineAddress(String searchText, int size) {

        Pageable pageable = PageRequest.of(0, size);
        List<Address> addresses = addressRepository.findByLabelIgnoreCaseContaining(searchText, pageable);

        AddressLine addressLine = new AddressLine();
        addressLine.setNumResults(addresses.size());
        addressLine.setAddresses(addresses);
        return addressLine;
    }

    public CompanyStructureDetailsResponseContent getReponseFromSymphony(String companyIndex) {

        return companyStructureDetailsResponseContentRepository.findByCompanyIndex(companyIndex);
    }

    public Iterable<AddressDetail> getAll() {
        return addressDetailsRepository.findAll();
    }

    public AddressDetail getByUnique(String uniqueId) {
        return addressDetailsRepository.findByAddressUniqueAddressId(Long.valueOf(uniqueId));
    }
}
