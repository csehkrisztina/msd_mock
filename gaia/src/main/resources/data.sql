INSERT INTO address (label, unique_address_id) VALUES
('Swisscom Liebefeld, Bernstrasse .5,  9200 Bernaaaaaaaaa', 721129123),
('Swisscom NE Privat, 8560 Zurich', 784551125),
('Infra Tower, Barenwelden .25,  3200 Bern', 965854125),
('Gutenberg, Humfastr .5,  7200 Bern' , 742235756),
('HPE PostParc, Banhofstr .17, 8340 Zurich', 42119961),
('Bahnhofstr. 4, 9602 Bazenheid', 66619961),
('Swisscom NE Gewerbe, 8020 Zurich', 102945905),
('Mariastrasse 81, 4569 Bern', 47885436),
('Buhlstrasswe 25, 3012 Bern', 65958214),
('Erlachstrasse 19,  Bern', 36552144),
('Maulbeerstrasse 13, 3025 Bern', 54128963),
('Effingerstrasse 74, 3021 Bern', 985567412),
('Munchenstrasse 14, 5697 Zurich', 985564542),
('Limatstrasse nr. 42, 8544 Zurich', 421511425),
('Langensteinstrasse 144, 5699 Zurich', 125454658),
('Zangenwerg 124, 8554 Zurich', 363211475),
('Zeppelinstrasse 4, 8633 Zurich', 985567412),
('Seminarstrasse 19, 5221 Zurich', 364885544),
('Furstweg 79, 6355 Zurich', 456789852);


INSERT INTO address_detail(id) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19);


INSERT INTO detail (address_line1, address_line2, address_line3, best_address_id, country, country_code, unique_address_id, address_detail_id) VALUES
('Bernstrasse .5', '9200 Bernaaaaaaaaa', null , 721129123, 'Schweiz', 'CHE', 721129123, 1 ),
('Swisscom NE Privat','8560 Zurich', null , 784551125, 'Schweiz', 'CHE', 784551125, 2 ),
('Infra Tower','Barenwelden .25,  3200 Bern', null , 965854125, 'Schweiz', 'CHE', 965854125, 3 ),
('Gutenberg, Humfastr .5','7200 Bern', null , 742235756, 'Schweiz', 'CHE', 742235756, 4 ),
('HPE PostParc, Banhofstrasse .17','8340 Zurich', null , 42119961, 'Schweiz', 'CHE', 42119961, 5 ),
('Bahnhofstrasse. 4',' 9602 Bazenheid', null , 66619961, 'Schweiz', 'CHE', 66619961, 6 ),
('Swisscom NE Gewerbe',' 8020 Zurich', null , 102945905, 'Schweiz', 'CHE', 102945905, 7 ),
('Mariastrasse 81',' 4569 Bern', null , 47885436, 'Schweiz', 'CHE', 47885436, 8 ),
('Buhlstrasswe 25',' 3012 Bern', null , 65958214, 'Schweiz', 'CHE', 65958214, 9 ),
('Erlachstrasse 19',' 3015 Bern', null , 36552144, 'Schweiz', 'CHE', 36552144, 10 ),
('Maulbeerstrasse 13',' 3025 Bern', null , 54128963, 'Schweiz', 'CHE', 54128963, 11 ),
('Effingerstrasse 74',' 3021 Bern', null , 985567412, 'Schweiz', 'CHE', 985567412, 12 ),
('Munchenstrasse 14,',' 5697 Zurich', null , 985564542, 'Schweiz', 'CHE', 985564542, 13 ),
('Limatstrasse nr. 42', '8544 Zurich', null , 421511425, 'Schweiz', 'CHE', 421511425, 14 ),
('Langensteinstrasse 144', '5699 Zurich', null , 125454658, 'Schweiz', 'CHE', 125454658, 15 ),
('Zangenwerg 124', '8554 Zurich', null , 363211475, 'Schweiz', 'CHE', 363211475, 16 ),
('Zeppelinstrasse 4', '8633 Zurich', null , 985567412, 'Schweiz', 'CHE', 985567412, 17 ),
('Seminarstrasse 19', '5221 Zurich', null , 364885544, 'Schweiz', 'CHE', 364885544, 18 ),
('Furstweg 79', '6355 Zurich', null , 456789852, 'Schweiz', 'CHE', 456789852, 19 );


INSERT INTO location (position, address_state, address_type, address_usage, best_coordinates, egid_unique, house_name, house_number, number_of_appart_bfs, number_of_app, number_of_appart_post,
      number_of_apb, number_Of_Asp, number_of_asb, second_supplier_code, street_language, street_language_snd, street_name, street_name_snd, street_short_name, street_short_name_snd, supplier_code, address_detail_id) VALUES
(null , 5, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Bernstrasse .5', null , 'Bernstr .5', null , '76CAM', 1),
(null , 5, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Swisscom NE Privat', null , 'Swisscom NE priv', null , '46CAM', 2),
(null , 5, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Barenwelden .25', null , 'Barenwld .25', null , '76CAM', 3),
(null , 5, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Humfastr .5', null , 'Humf .5', null , '41ABM', 4),
(null , 5, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Banhofstrasse .17', null , 'Banhofstr .17', null , '81CAM', 5),
(null , 5, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Bahnhofstrasse. 4', null , 'Bahnhofstr. 4', null , '16ATM', 6),
(null , 5, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Swisscom NE Gewerbe', null , 'Swisscom NE gwb', null , '76CAM', 7),
(null , 5, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Mariastrasse 81', null , 'Mariastr 81', null , '14TMS', 8),
(null , 4, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Buhlstrasswe 25', null , 'Buhlstr 25', null , '58CAM', 9),
(null , 4, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Erlachstrasse 19', null , 'Erlachstr 19b', null , '76CAM', 10),
(null , 1, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Maulbeerstrasse 13', null , 'Maulbeerstr 13', null , '48DIM', 11),
(null , 3, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Effingerstrasse 74', null , 'Effingerstr 74', null , '36LAM', 12),
(null , 5, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Munchenstrasse 14', null , 'Munchenstr 14', null , '36LAM', 13),
(null , 5, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Limatstrasse nr. 42', null , 'Limatstr nr. 42', null , '65FDA', 14),
(null , 3, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Langensteinstrasse 144', null , 'Langensteinstr 144', null , '78GHA', 15),
(null , 3, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Zangenwerg 124', null , 'Zangenwerg 124', null , '19KJH', 16),
(null , 4, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Zeppelinstrasse 4', null , 'Zeppelinstr 4', null , '35TRA', 17),
(null , 2, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Seminarstrasse 19', null , 'Seminarstr 19', null , '33DDA', 18),
(null , 3, 1, 'P', null ,null , null, null , null , null , null , null , null , null , null , 'FR', null , 'Furstweg 79', null , 'Furstweg 79', null , '31FAM', 19);


INSERT INTO bfs (canton_code, coordinates, edid, egid, gegrid, house_name, house_number, land_register_number, lot_number, municipality_name, municipality_nr, number_of_appart, street_language, street_language_snd, street_name, street_name_snd, address_detail_id) VALUES
(null, null, null, null, null, null, null, null, null , 'Bern', 9200, null , 'FR', null , 'Bernstrasse .5', null , 1),
(null, null, null, null, null, null, null, null, null , 'Zurich', 8560, null , 'FR', null , 'Swisscom NE Privat', null , 2),
(null, null, null, null, null, null, null, null, null , 'Bern', 3200, null , 'FR', null , 'Barenwelden .25', null , 3),
(null, null, null, null, null, null, null, null, null , 'Bern', 7200, null , 'FR', null , 'Humfastr .5', null , 4),
(null, null, null, null, null, null, null, null, null , 'Zurich', 8340, null , 'FR', null , 'Banhofstrasse .17', null , 5),
(null, null, null, null, null, null, null, null, null , 'Bazenheid', 9602, null , 'FR', null , 'Bahnhofstrasse .4', null , 6),
(null, null, null, null, null, null, null, null, null , 'Zurich', 8020, null , 'FR', null , 'Swisscom NE Gewerbe', null , 7),
(null, null, null, null, null, null, null, null, null , 'Bern', 4569, null , 'FR', null , 'Mariastrasse 81', null , 8),
(null, null, null, null, null, null, null, null, null , 'Bern', 3012, null , 'FR', null , 'Buhlstrasswe 25', null , 9),
(null, null, null, null, null, null, null, null, null , 'Bern', 3015, null , 'FR', null , 'Erlachstrasse 19', null , 10),
(null, null, null, null, null, null, null, null, null , 'Bern', 3025, null , 'FR', null , 'Maulbeerstrasse 13', null , 11),
(null, null, null, null, null, null, null, null, null , 'Bern', 3021, null , 'FR', null , 'Effingerstrasse 74', null , 12),
(null, null, null, null, null, null, null, null, null , 'Zurich', 5697, null , 'FR', null , 'Munchenstrasse 14', null , 13),
(null, null, null, null, null, null, null, null, null , 'Zurich', 8544, null , 'FR', null , 'Limatstrasse nr. 42', null , 14),
(null, null, null, null, null, null, null, null, null , 'Zurich', 5699, null , 'FR', null , 'Langensteinstrasse 144', null , 15),
(null, null, null, null, null, null, null, null, null , 'Zurich', 8554, null , 'FR', null , 'Zangenwerg 124', null , 16),
(null, null, null, null, null, null, null, null, null , 'Zurich', 8633, null , 'FR', null , 'Zeppelinstrasse 4', null , 17),
(null, null, null, null, null, null, null, null, null , 'Zurich', 5221, null , 'FR', null , 'Seminarstrasse 19', null , 18),
(null, null, null, null, null, null, null, null, null , 'Zurich', 6355, null , 'FR', null , 'Furstweg 79', null , 19);


INSERT INTO postal (canton_code, coordinates, country, geo_post_key, house_name, house_number, municipality_name, municipality_nr, number_of_appart, place_name,
place_short_name, region, street_language, street_language_snd, street_name, street_name_snd, zip_code6, zip_type, address_detail_id ) VALUES
('VS', null , 'CHE', null , null , null , 'Bern', 9200, null , 'Bern', 'Bern', null , 'FR', null , 'Bernstrasse .5' , null, 195700, 10, 1),
('AD', null , 'CHE', null , null , null , 'Zurich', 8560, null , 'Zurich', 'Zurich', null , 'FR', null , 'Swisscom NE Privat' , null, 135670, 10, 2),
('DT', null , 'CHE', null , null , null , 'Bern', 3200, null , 'Bern', 'Bern', null , 'FR', null , 'Barenwelden .25' , null, 132554, 10, 3),
('ZH', null , 'CHE', null , null , null , 'Bern', 7200, null , 'Bern', 'Bern', null , 'FR', null , 'Humfastr .5' , null, 197885, 10, 4),
('CE', null , 'CHE', null , null , null , 'Zurich', 8340, null , 'Zurich', 'Zurich', null , 'FR', null , 'Banhofstrasse .17' , null, 136654, 10, 5),
('VT', null , 'CHE', null , null , null , 'Bazenheid', 9602, null , 'Bazenheid', 'Bazenheid', null , 'FR', null , 'Bahnhofstrasse .4' , null, 195700, 10, 6),
('SV', null , 'CHE', null , null , null , 'Zurich', 8020, null , 'Zurich', 'Zurich', null , 'FR', null , 'Swisscom NE Gewerbe' , null, 121458, 10, 7),
('SV', null , 'CHE', null , null , null , 'Bern', 4569, null , 'Bern', 'Bern', null , 'FR', null , 'Mariastrasse 81' , null, 652147, 10, 8),
('TD', null , 'CHE', null , null , null , 'Bern', 3012, null , 'Bern', 'Bern', null , 'FR', null , 'Buhlstrasswe 25' , null, 145558, 10, 9),
('VD', null , 'CHE', null , null , null , 'Bern', 3015, null , 'Bern', 'Bern', null , 'FR', null , 'Erlachstrasse 19' , null, 163552, 10, 10),
('ST', null , 'CHE', null , null , null , 'Bern', 3025, null , 'Bern', 'Bern', null , 'FR', null , 'Maulbeerstrasse 13' , null, 145774, 10, 11),
('SE', null , 'CHE', null , null , null , 'Bern', 3021, null , 'Bern', 'Bern', null , 'FR', null , 'Effingerstrasse 74' , null, 114475, 10, 12),
('VS', null , 'CHE', null , null , null , 'Bern', 3021, null , 'Zurich', 'Zurich', null , 'FR', null , 'Munchenstrasse 14' , null, 155224, 10, 13),
('SV', null , 'CHE', null , null , null , 'Bern', 3021, null , 'Zurich', 'Zurich', null , 'FR', null , 'Limatstrasse nr. 42' , null, 189965, 10, 14),
('SE', null , 'CHE', null , null , null , 'Bern', 3021, null , 'Zurich', 'Zurich', null , 'FR', null , 'Langensteinstrasse 144' , null, 163321, 10, 15),
('ST', null , 'CHE', null , null , null , 'Bern', 3021, null , 'Zurich', 'Zurich', null , 'FR', null , 'Zangenwerg 124' , null, 232210, 10, 16),
('DH', null , 'CHE', null , null , null , 'Bern', 3021, null , 'Zurich', 'Zurich', null , 'FR', null , 'Zeppelinstrasse 4' , null, 410002, 10, 17),
('DF', null , 'CHE', null , null , null , 'Bern', 3021, null , 'Zurich', 'Zurich', null , 'FR', null , 'Seminarstrasse 19' , null, 144225, 10, 18),
('ER', null , 'CHE', null , null , null , 'Bern', 3021, null , 'Zurich', 'Zurich', null , 'FR', null , 'Furstweg 79' , null, 131120, 10, 19);


INSERT INTO company_structure_details_response_content(id, company_index) VALUES
(1, '4123423'),
(2, '1231233'),
(3, '4112312'),
(4, '4123533'),
(5, '4126523'),
(6, '5443423');


INSERT INTO CUSTOMER(COMPANY_STRUCTURE_DETAILS_RESPONSE_CONTENT_ID) VALUES
(1),
(2),
(3),
(4),
(5),
(5);


INSERT INTO identifier(value,CUSTOMER_ID) VALUES
('1234', 1),
('2233', 2),
('4433', 3),
('1122', 4),
('3412', 5),
('6677', 5);


INSERT INTO ORGANIZATION_UNIT(HEAD_QUARTER) VALUES
(false),
(true),
(true),
(true),
(false),
(false);


INSERT INTO ORGANIZATION_NAME(ORGANIZATION_UNIT_ID) VALUES
(1),
(2),
(3),
(4),
(5),
(6);


INSERT INTO SYMPHONY_ADDRESS(LOCATION_ID, ZIP_CODE, VALIDATION_STATUS, CITY, STREET_NAME, COUNTRY, HOUSE_NUMBER, HOUSE_NAME, LOCATION_SUPPLEMENT, CUSTOMER_ID) VALUES
(721129123, '4412', 2, 'Bernaaaaaa', 'Swisscom NE Privat', 'CH', '2' , null , null , 1),
(784551125, '4412', 2, 'Bern', 'Swisscom NE Privat', 'CH', '2' , null , null , 2),
(421511425, '4412', 2, 'Bern', 'Swisscom NE Privat', 'CH', '2' , null , null , 3),
(363211475, '5885', 2, 'Bern', 'Swisscom NE Privat', 'CH', '2' , null , null , 4),
(784551125, '4412', 2, 'Bern', 'Swisscom NE Privat', 'CH', '2' , null , null , 5),
(985567412, '4412', 2, 'Bern', 'Swisscom NE Privat', 'CH', '2' , null , null , 6);