package com.msd.gaia.controller;

import com.msd.gaia.entity.AddressDetail;
import com.msd.gaia.entity.AddressLine;
import com.msd.gaia.service.GaiaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static com.msd.gaia.mocks.MockGaiaResponse.mockAddressDetailResponse;
import static com.msd.gaia.mocks.MockGaiaResponse.mockAddressLineResponse;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(GaiaController.class)
public class GaiaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GaiaService gaiaService;

    @SuppressWarnings("unused")
    @InjectMocks
    private GaiaController gaiaController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getLocationAddressLineReturn200MultipleResults() throws Exception {

        AddressLine addressLine = mockAddressLineResponse(3);
        when(gaiaService.getResponesFrom1LineAddress(eq("Ber"), eq(3))).thenReturn(addressLine);

        // Perform GET at the correct URL template
        mockMvc
                .perform(get("/api/location/addressLine/search?searchText=Ber&maxResults=3&userId=MsdGaia").header("Authorization", "AbCdEf123456" ))
                .andExpect(status().is(200))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$").hasJsonPath())
                .andExpect(jsonPath("$.addresses", hasSize(greaterThanOrEqualTo(0))))
                .andExpect(jsonPath("$.numResults").hasJsonPath())
                .andExpect(jsonPath("$.addresses.[*]", hasSize(greaterThanOrEqualTo(0))))
                .andExpect(jsonPath("$.addresses.[*]").hasJsonPath())
                .andExpect(jsonPath("$.addresses.[*].label", hasSize(greaterThanOrEqualTo(0))))
                .andExpect(jsonPath("$.addresses.[*].label").hasJsonPath())
                .andReturn();
    }

    @Test
    public void getLocationAddressLineReturn400BadRequest() throws Exception {

        AddressLine addressLine = mockAddressLineResponse(3);
        when(gaiaService.getResponesFrom1LineAddress(eq("Ber"), eq(3))).thenReturn(addressLine);

        // Perform GET without the authorization header present
        mockMvc
                .perform(get("/api/location/addressLine/search?searchText=Ber&maxResults=3"))
                .andExpect(status().is(400))
                .andReturn();
    }

    @Test
    public void getLocationAddressLineReturn404NotFound() throws Exception {

        AddressLine addressLine = mockAddressLineResponse(3);
        when(gaiaService.getResponesFrom1LineAddress(eq("Ber"), eq(3))).thenReturn(addressLine);

        // Perform GET at a wrong URL template
        mockMvc
                .perform(get("/api/location/addresssLine/search?searchText=Ber&maxResults=3"))
                .andExpect(status().is(404))
                .andReturn();
    }


    @Test
    public void getLocationAddressLineReturn401Unauthorized() throws Exception {

        AddressLine addressLine = mockAddressLineResponse(3);
        when(gaiaService.getResponesFrom1LineAddress(eq("Ber"), eq(3))).thenReturn(addressLine);

        // Perform GET with wrong authorization header value(bad credentials)
        mockMvc
                .perform(get("/api/location/addressLine/search?searchText=Ber&maxResults=3&userId=MsdGaia").header("Authorization", "AbCdEf123456aas" ))
                .andExpect(status().is(401))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void getLocationAddressLineReturn403Forbidden() throws Exception {

        AddressLine addressLine = mockAddressLineResponse(3);
        when(gaiaService.getResponesFrom1LineAddress(eq("Ber"), eq(3))).thenReturn(addressLine);

        // Perform GET with valid credentials but no permissions
        mockMvc
                .perform(get("/api/location/addressLine/search?searchText=Ber&maxResults=3&userId=MsdGaia").header("Authorization", "DFrgEf123456" ))
                .andExpect(status().is(403))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void getLocationAddressLineReturn405InvalidRequest() throws Exception {

        AddressLine addressLine = mockAddressLineResponse(3);
        when(gaiaService.getResponesFrom1LineAddress(eq("Ber"), eq(3))).thenReturn(addressLine);

        // Perform POST at the valid URL should return 405
        mockMvc
                .perform(post("/api/location/addressLine/search?searchText=Ber&maxResults=3").header("Authorization", "DFrgEf123456" ))
                .andExpect(status().is(405))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }



    @Test
    public void getLocationAddressDetailReturn200() throws Exception {

        AddressDetail addressDetail = mockAddressDetailResponse(3);
        when(gaiaService.getByUnique(eq("721129123"))).thenReturn(addressDetail);

        // Perform GET at the correct URL template
        mockMvc
                .perform(get("/api/location/addressDetails?uniqueId=721129123&userId=MsdGaia").header("Authorization", "AbCdEf123456" ))
                .andExpect(status().is(200))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$").hasJsonPath())
                .andExpect(jsonPath("$.address").hasJsonPath())
                .andExpect(jsonPath("$.bfs").hasJsonPath())
                .andExpect(jsonPath("$.location.[*]").hasJsonPath())
                .andExpect(jsonPath("$.postal.[*]").hasJsonPath())
                .andReturn();
    }

    @Test
    public void getLocationAddressDetailReturn400BadRequest() throws Exception {

        AddressDetail addressDetail = mockAddressDetailResponse(3);
        when(gaiaService.getByUnique(eq("721129123"))).thenReturn(addressDetail);

        // Perform GET without the authorization header present
        mockMvc
                .perform(get("/api/location/addressDetails?uniqueId=721129123&userId=MsdGaia"))
                .andExpect(status().is(400))
                .andReturn();
    }

    @Test
    public void getLocationAddressDetailsReturn404NotFound() throws Exception {

        AddressDetail addressDetail = mockAddressDetailResponse(3);
        when(gaiaService.getByUnique(eq("721129123"))).thenReturn(addressDetail);

        // Perform GET at a wrong URL template
        mockMvc
                .perform(get("/api/location/addresssLine/search?searchText=Ber&maxResults=3"))
                .andExpect(status().is(404))
                .andReturn();
    }


    @Test
    public void getLocationAddressDetailsReturn401Unauthorized() throws Exception {

        AddressDetail addressDetail = mockAddressDetailResponse(3);
        when(gaiaService.getByUnique(eq("721129123"))).thenReturn(addressDetail);

        // Perform GET with wrong authorization header value(bad credentials)
        mockMvc
                .perform(get("/api/location/addressDetails?uniqueId=721129123&userId=MsdGaia").header("Authorization", "AbCdEf123456aas" ))
                .andExpect(status().is(401))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void getLocationAddressDetailReturn403Forbidden() throws Exception {

        AddressDetail addressDetail = mockAddressDetailResponse(3);
        when(gaiaService.getByUnique(eq("721129123"))).thenReturn(addressDetail);

        // Perform GET with valid credentials but no permissions
        mockMvc
                .perform(get("/api/location/addressDetails?uniqueId=721129123&userId=MsdGaia").header("Authorization", "DFrgEf123456" ))
                .andExpect(status().is(403))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void getLocationAddressDetailReturn405InvalidRequest() throws Exception {

        AddressDetail addressDetail = mockAddressDetailResponse(3);
        when(gaiaService.getByUnique(eq("721129123"))).thenReturn(addressDetail);

        // Perform POST at the valid URL should return 405
        mockMvc
                .perform(post("/api/location/addressDetails?uniqueId=721129123&userId=MsdGaia").header("Authorization", "DFrgEf123456" ))
                .andExpect(status().is(405))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}