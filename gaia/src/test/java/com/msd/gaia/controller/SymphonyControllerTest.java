package com.msd.gaia.controller;

import com.msd.gaia.mocks.MockSymphonyResponse;
import com.msd.gaia.entity.CompanyStructureDetailsResponseContent;
import com.msd.gaia.service.GaiaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(GaiaController.class)
public class SymphonyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GaiaService gaiaService;

    @SuppressWarnings("unused")
    @InjectMocks
    private GaiaController locationController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private String xml = "<v1:getCompanyStructureDetailsRequest xmlns:v1=\"http://www.swisscom.ch/eai/bsl/customer/v1-0\">\n" +
            "    <v1:trackingInformation  xmlns:typ=\"http://www.swisscom.ch/eai/bsl/generic/types\">\n" +
            "        <typ:eventLocalID>234233423434234234></typ:eventLocalID>\n" +
            "        <typ:applicationID>MSD</typ:applicationID>\n" +
            "        <typ:originApplicationID>MSD</typ:originApplicationID>\n" +
            "    </v1:trackingInformation>\n" +
            "    <v1:getCompanyStructureDetailsRequestContent>\n" +
            "        <v1:customer>\n" +
            "            <v1:organizationIdentifier>\n" +
            "                <v1:companyIndex>1231233</v1:companyIndex>\n" +
            "            </v1:organizationIdentifier>\n" +
            "        </v1:customer>\n" +
            "    </v1:getCompanyStructureDetailsRequestContent>\n" +
            "</v1:getCompanyStructureDetailsRequest>";


    @Test
    public void getSymphonyAddressReturn200OneResult() throws Exception {

        CompanyStructureDetailsResponseContent companyStructureDetailsResponseContent = MockSymphonyResponse.mockSymphonyResponse(3);
        when(gaiaService.getReponseFromSymphony(anyString())).thenReturn(companyStructureDetailsResponseContent);

        // Perform GET at the correct URL template
        mockMvc
                .perform(post("/api/symphony/customer/lookup").header("Authorization", "AbCdEf123456" ).content(xml).contentType(MediaType.APPLICATION_ATOM_XML_VALUE))
                .andExpect(status().is(200))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(content().contentType(MediaType.parseMediaType("application/atom+xml;charset=UTF-8")))
                .andExpect(xpath("getCompanyStructureDetailsResponseContent").exists())
                .andExpect(xpath("getCompanyStructureDetailsResponseContent").nodeCount(1))
                .andReturn();
    }

    @Test
    public void getSymphonyAddressReturn400BadRequest() throws Exception {

        CompanyStructureDetailsResponseContent companyStructureDetailsResponseContent = MockSymphonyResponse.mockSymphonyResponse(3);
        when(gaiaService.getReponseFromSymphony(anyString())).thenReturn(companyStructureDetailsResponseContent);

        // Perform GET without the authorization header present
        mockMvc
                .perform(post("/api/symphony/customer/lookup").content(xml).contentType(MediaType.APPLICATION_ATOM_XML_VALUE))
                .andExpect(status().is(400))
                .andReturn();
    }

    @Test
    public void getSymphonyAddressReturn404NotFound() throws Exception {

        CompanyStructureDetailsResponseContent companyStructureDetailsResponseContent = MockSymphonyResponse.mockSymphonyResponse(3);
        when(gaiaService.getReponseFromSymphony(anyString())).thenReturn(companyStructureDetailsResponseContent);

        // Perform GET at a wrong URL template
        mockMvc
                .perform(post("/api/symphony/customer/lookups").content(xml).contentType(MediaType.APPLICATION_ATOM_XML_VALUE))
                .andExpect(status().is(404))
                .andReturn();
    }


    @Test
    public void getSymphonyAddressReturn401Unauthorized() throws Exception {

        CompanyStructureDetailsResponseContent companyStructureDetailsResponseContent = MockSymphonyResponse.mockSymphonyResponse(3);
        when(gaiaService.getReponseFromSymphony(anyString())).thenReturn(companyStructureDetailsResponseContent);

        // Perform GET with wrong authorization header value(bad credentials)
        mockMvc
                .perform(post("/api/symphony/customer/lookup").header("Authorization", "ds3434fds" ).content(xml).contentType(MediaType.APPLICATION_ATOM_XML_VALUE))
                .andExpect(status().is(401))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void getSymphonyAddressReturn403Forbidden() throws Exception {

        CompanyStructureDetailsResponseContent companyStructureDetailsResponseContent = MockSymphonyResponse.mockSymphonyResponse(3);
        when(gaiaService.getReponseFromSymphony(anyString())).thenReturn(companyStructureDetailsResponseContent);

        // Perform GET with valid credentials but no permissions
        mockMvc
                .perform(post("/api/symphony/customer/lookup").content(xml).contentType(MediaType.APPLICATION_ATOM_XML_VALUE).header("Authorization", "DFrgEf123456" ))
                .andExpect(status().is(403))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void getSymphonyAddressReturn405InvalidRequest() throws Exception {

        CompanyStructureDetailsResponseContent companyStructureDetailsResponseContent = MockSymphonyResponse.mockSymphonyResponse(3);
        when(gaiaService.getReponseFromSymphony(anyString())).thenReturn(companyStructureDetailsResponseContent);

        // Perform GET at the valid URL should return 405
        mockMvc
                .perform(get("/api/symphony/customer/lookup").content(xml).contentType(MediaType.APPLICATION_ATOM_XML_VALUE).header("Authorization", "AbCdEf123456aas" ))
                .andExpect(status().is(405))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}
