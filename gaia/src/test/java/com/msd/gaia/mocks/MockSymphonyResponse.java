package com.msd.gaia.mocks;

import com.msd.gaia.entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SuppressWarnings("SameParameterValue")
public class MockSymphonyResponse {

    public static CompanyStructureDetailsResponseContent mockSymphonyResponse(int results) {
        return symphonyAddressList(results);
    }

    private static CompanyStructureDetailsResponseContent symphonyAddressList(int results) {

        CompanyStructureDetailsResponseContent companyStructureDetailsResponseContent = new CompanyStructureDetailsResponseContent();
        List<Customer> customerList = new ArrayList<>();
        for(int i=0; i< results; i++) {
            Customer customer = new Customer();
            Identifier identifier = new Identifier();
            identifier.setValue(generateRandomInteger(4));
            customer.setIdentifier(identifier);
            customer.setStatus(null);
            customer.setOrganizationUnit(null);
            SymphonyAddress symphonyAddress = new SymphonyAddress();
            symphonyAddress.setLocationId(Long.parseLong(generateRandomInteger(4)));
            symphonyAddress.setZipCode(Integer.parseInt(generateRandomInteger(4)));
            symphonyAddress.setValidationStatus(2);
            symphonyAddress.setCity(generateRandomWords(1));
            symphonyAddress.setStreetName(generateRandomWords(3));
            symphonyAddress.setCountry(generateRandomWords(1));
            symphonyAddress.setHouseNumber(Integer.parseInt(generateRandomInteger(1)));
            symphonyAddress.setHouseName(null);
            symphonyAddress.setLocationSupplement(null);
            customer.setAddress(symphonyAddress);
            customerList.add(customer);
        }
        companyStructureDetailsResponseContent.setCustomers(customerList);
        return companyStructureDetailsResponseContent;
    }

    private static String generateRandomInteger(int length){

        Random r = new Random();
        int Low = 1;
        int High = 10;
        int rand;
        String concatenatedString = "";

        for(int i = 0; i < length; i++) {
            rand = r.nextInt(High-Low) + Low;
            concatenatedString = concatenatedString.concat(Integer.toString(rand));
        }

        return concatenatedString;

    }

    private static String generateRandomWords(int numberOfWords)
    {
        String[] randomStrings = new String[numberOfWords];
        Random random = new Random();
        for(int i = 0; i < numberOfWords; i++)
        {
            char[] word = new char[random.nextInt(8)+3];
            for(int j = 0; j < word.length; j++)
            {
                word[j] = (char)('a' + random.nextInt(26));
            }
            randomStrings[i] = new String(word);
        }
        return String.join(" ", randomStrings);
    }

}
