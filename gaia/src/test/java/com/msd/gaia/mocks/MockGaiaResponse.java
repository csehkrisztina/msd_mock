package com.msd.gaia.mocks;

import com.msd.gaia.entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SuppressWarnings("SameParameterValue")
public class MockGaiaResponse {


    public static AddressLine mockAddressLineResponse(int results) {
        return addressList(results);
    }

    public static AddressDetail mockAddressDetailResponse(int results) {
        return addressDetail(results);
    }

    private static AddressLine addressList(int results) {

        AddressLine addressLine = new AddressLine();
        List<Address> addresses = new ArrayList<>();
        for(int i=0; i< results; i++) {
            Address address = new Address();
            address.setUniqueAddressId(Long.valueOf(generateRandomInteger(8)));
            address.setLabel(generateRandomWords(3) + " nr." + generateRandomInteger(2));
            addresses.add(address);
            addressLine.setAddresses(addresses);
            addressLine.setNumResults(results);
        }
        return addressLine;
    }

    private static AddressDetail addressDetail(int results) {

        AddressDetail addressDetail = new AddressDetail();
        List<Address> addresses = new ArrayList<>();
        for(int i=0; i< results; i++) {
            Detail address = new Detail();
            address.setAddressLine1(generateRandomWords(4));
            address.setAddressLine2(generateRandomInteger(4) + " " + generateRandomWords(1));
            address.setAddressLine3(null);
            address.setUniqueAddressId(Long.valueOf(generateRandomInteger(8)));
            address.setBestAddressId(Long.valueOf(generateRandomInteger(8)));
            address.setCountry(generateRandomWords(1));
            address.setCountryCode(generateRandomWords(1));
            Location location = new Location();
            location.setAddressState(Integer.valueOf(generateRandomInteger(1)));
            location.setAddressUsage(generateRandomWords(1));
            location.setAddressType(Integer.valueOf(generateRandomInteger(1)));
            location.setStreetName(address.getAddressLine1());
            location.setStreetShortName(generateRandomWords(2));
            location.setStreetLanguage(generateRandomWords(1));
            location.setStreetNameSnd(null);
            location.setStreetLanguageSnd(null);
            location.setStreetShortNameSnd(null);
            location.setHouseName(null);
            location.setHouseNumber(null);
            location.setNumberOfAppartBfS(null);
            location.setNumberOfAppartPost(null);
            location.setBestCoordinates(null);
            location.setSupplierCode(generateRandomWords(1));
            location.setSecondSupplierCode(null);
            location.setEgidUnique("0");
            location.setPosition(null);
            Bfs bfs = new Bfs();
            bfs.setEdid(null);
            bfs.setEgid(null);
            bfs.setMunicipalityName(generateRandomWords(1));
            bfs.setMunicipalityNr(null);
            bfs.setLotNumber(null);
            bfs.setLandRegisterNumber(null);
            bfs.setGegrid(null);
            bfs.setCantonCode(null);
            bfs.setStreetName(generateRandomWords(4));
            bfs.setStreetLanguage(generateRandomWords(1));
            bfs.setStreetNameSnd(null);
            bfs.setStreetLanguageSnd(null);
            bfs.setHouseName(null);
            bfs.setHouseNumber(null);
            bfs.setCoordinates(null);
            bfs.setNumberOfAppart(null);
            Postal postal = new Postal();
            postal.setCantonCode(generateRandomWords(1));
            postal.setCountry(generateRandomWords(1));
            postal.setZipCode6(generateRandomWords(1));
            postal.setZipType(generateRandomInteger(2));
            postal.setPlaceName(generateRandomWords(1));
            postal.setPlaceShortName(generateRandomWords(1));
            postal.setMunicipalityNr(generateRandomInteger(4));
            postal.setMunicipalityName(generateRandomWords(1));
            postal.setStreetName(generateRandomWords(4));
            postal.setStreetLanguage(generateRandomWords(1));
            postal.setStreetLanguageSnd(null);
            postal.setStreetNameSnd(null);
            postal.setHouseName(null);
            postal.setHouseNumber(null);
            postal.setNumberOfAppart(null);
            postal.setGeoPostKey(null);
            postal.setCoordinates(null);
            addressDetail.setAddress(address);
            addressDetail.setBfs(bfs);
            addressDetail.setLocation(location);
            addressDetail.setPostal(postal);
        }
        return addressDetail;
    }
    private static String generateRandomInteger(int length){

        Random r = new Random();
        int Low = 1;
        int High = 10;
        int rand;
        String concatenatedString = "";

        for(int i = 0; i < length; i++) {
            rand = r.nextInt(High-Low) + Low;
            concatenatedString = concatenatedString.concat(Integer.toString(rand));
        }

        return concatenatedString;

    }

    private static String generateRandomWords(int numberOfWords)
    {
        String[] randomStrings = new String[numberOfWords];
        Random random = new Random();
        for(int i = 0; i < numberOfWords; i++)
        {
            char[] word = new char[random.nextInt(8)+3];
            for(int j = 0; j < word.length; j++)
            {
                word[j] = (char)('a' + random.nextInt(26));
            }
            randomStrings[i] = new String(word);
        }
        return String.join(" ", randomStrings);
    }
}
